package com.novapat.proposta.appcartoes.cliente.service;

import com.novapat.proposta.appcartoes.cliente.model.Cliente;

public interface ClienteService {

    Cliente save(Cliente cliente);

    Cliente findById(long idCliente);
}
