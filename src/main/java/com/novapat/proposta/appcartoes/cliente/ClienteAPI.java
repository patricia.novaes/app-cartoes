package com.novapat.proposta.appcartoes.cliente;

import com.novapat.proposta.appcartoes.cliente.model.dto.request.ClienteCreateRequest;
import com.novapat.proposta.appcartoes.cliente.model.ClienteMapper;
import com.novapat.proposta.appcartoes.cliente.model.Cliente;
import com.novapat.proposta.appcartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClienteAPI {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping(path = "/cliente", consumes = "application/json")
    public ResponseEntity<?> createCard(@RequestBody ClienteCreateRequest clienteCreateRequest) {
        try {

            Cliente cliente = clienteMapper.criarCliente(clienteCreateRequest);
            cliente = clienteService.save(cliente);

            return new ResponseEntity<>(clienteMapper.criarClienteResponse(cliente), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/cliente/{id}")
    public ResponseEntity<?> createCard(@PathVariable(name = "id") long idCliente) {

        Cliente client = clienteService.findById(idCliente);
        return new ResponseEntity<>(clienteMapper.criarClienteResponse(client), HttpStatus.OK);

    }

}
