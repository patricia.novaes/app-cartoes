package com.novapat.proposta.appcartoes.cliente.service.impl;

import com.novapat.proposta.appcartoes.cliente.exception.ClienteNotFoundException;
import com.novapat.proposta.appcartoes.cliente.model.Cliente;
import com.novapat.proposta.appcartoes.cliente.repository.ClienteRepository;
import com.novapat.proposta.appcartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente save(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @Override
    public Cliente findById(long idCliente) {
        Optional<Cliente> client = clienteRepository.findById(idCliente);

        if (!client.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return client.get();
    }
}
