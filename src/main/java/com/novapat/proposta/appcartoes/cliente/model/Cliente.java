package com.novapat.proposta.appcartoes.cliente.model;

import com.novapat.proposta.appcartoes.cartoes.model.Cartao;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "nomecliente")
    private String name;

    @OneToMany
    private List<Cartao> cartaoList;


    public long getId() {
        return id;
    }

    public void setId(long idCliente) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Cartao> getCartaoList() {
        return cartaoList;
    }

    public void setCartaoList(List<Cartao> cartaoList) {
        this.cartaoList = cartaoList;
    }
}
