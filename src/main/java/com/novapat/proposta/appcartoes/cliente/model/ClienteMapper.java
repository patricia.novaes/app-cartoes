package com.novapat.proposta.appcartoes.cliente.model;

import com.novapat.proposta.appcartoes.cliente.model.dto.request.ClienteCreateRequest;
import com.novapat.proposta.appcartoes.cliente.model.dto.response.ClienteCreateResponse;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente criarCliente(ClienteCreateRequest request) {
        Cliente cliente = new Cliente();
        cliente.setName(request.getName());

        return cliente;
    }

    public ClienteCreateResponse criarClienteResponse(Cliente cliente) {
        ClienteCreateResponse response = new ClienteCreateResponse();
        response.setId(cliente.getId());
        response.setName(cliente.getName());

        return response;
    }

}
