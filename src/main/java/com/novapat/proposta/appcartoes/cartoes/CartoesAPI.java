package com.novapat.proposta.appcartoes.cartoes;

import com.novapat.proposta.appcartoes.cartoes.model.Cartao;
import com.novapat.proposta.appcartoes.cartoes.model.CartaoMapper;
import com.novapat.proposta.appcartoes.cartoes.model.dto.request.ChangeStatusCartaoRequest;
import com.novapat.proposta.appcartoes.cartoes.model.dto.request.CreateCartaoRequest;
import com.novapat.proposta.appcartoes.cartoes.service.CartoesService;
import com.novapat.proposta.appcartoes.cliente.model.Cliente;
import com.novapat.proposta.appcartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartoesAPI {

    @Autowired
    private CartoesService cartoesService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping(path = "/cartao", consumes = "application/json")
    public ResponseEntity<?> createCard(@RequestBody CreateCartaoRequest request) {
        try {
            Cartao cartao = cartaoMapper.criarCartao(request);
            Cliente cliente = clienteService.findById(request.getClienteId());
            cartao.setCliente(cliente);
            cartao = cartoesService.save(cartao);
            return new ResponseEntity<>(cartaoMapper.criarCartaoResponse(cartao), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getCause()+ " erro ao salvar o cartão";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping(path = "/cartao/{numero}", consumes = "application/json")
    public ResponseEntity<?> createCard(@PathVariable String numero, @RequestBody ChangeStatusCartaoRequest request) {
        try {
            Cartao cartao = new Cartao();
            cartao.setNumeroCartao(numero);
            cartao.setAtivo(request.isAtivo());

            Cartao savedCard = cartoesService.ativar(cartao);

            return new ResponseEntity<>(cartaoMapper.ativarCartao(savedCard), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== erro ao alterar o status do cartão";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/cartao/{numero}")
    public ResponseEntity<?> createCard(@PathVariable(name = "numero") String numero) {
        try {

            Cartao savedCard = cartoesService.findByNumeroCartao(numero);

            return new ResponseEntity<>(cartaoMapper.ativarCartao(savedCard), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== erro ao recuperar o cartao";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

}
