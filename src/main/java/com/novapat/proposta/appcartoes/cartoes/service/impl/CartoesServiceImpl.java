package com.novapat.proposta.appcartoes.cartoes.service.impl;

import com.novapat.proposta.appcartoes.cartoes.exception.CartaoNotFoundException;
import com.novapat.proposta.appcartoes.cartoes.model.Cartao;
import com.novapat.proposta.appcartoes.cartoes.repository.CartoesRepository;
import com.novapat.proposta.appcartoes.cartoes.service.CartoesService;
import com.novapat.proposta.appcartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartoesServiceImpl implements CartoesService {

    @Autowired
    private CartoesRepository cartoesRepository;

    @Autowired
    private ClienteService clienteService;

    @Override
    public Cartao save(Cartao cartao) throws Exception {
        clienteService.findById(cartao.getCliente().getId());
        return cartoesRepository.saveAndFlush(cartao);
    }

    @Override
    public Cartao ativar(Cartao cartao) {

        Cartao card = findByNumeroCartao(cartao.getNumeroCartao());
        card.setAtivo(cartao.isAtivo());

        return cartoesRepository.save(card);
    }

    @Override
    public Cartao findByNumeroCartao(String numeroCartao) {
        Optional<Cartao> cartao = cartoesRepository.findByNumeroCartao(numeroCartao);
        if (!cartao.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return cartao.get();
    }
}
