package com.novapat.proposta.appcartoes.cartoes.model;

import com.novapat.proposta.appcartoes.cartoes.model.dto.request.CreateCartaoRequest;
import com.novapat.proposta.appcartoes.cartoes.model.dto.response.CartaoDetailsResponse;
import com.novapat.proposta.appcartoes.cartoes.model.dto.response.ChangeStatusCartaoResponse;
import com.novapat.proposta.appcartoes.cartoes.model.dto.response.CreateCartaoResponse;
import com.novapat.proposta.appcartoes.cliente.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao criarCartao(CreateCartaoRequest request) {
        Cartao cartao = new Cartao();
        cartao.setNumeroCartao(request.getNumero());

        return cartao;
    }

    public CreateCartaoResponse criarCartaoResponse(Cartao cartao) {
        CreateCartaoResponse response = new CreateCartaoResponse();
        response.setId(cartao.getIdCartao());
        response.setAtivo(cartao.isAtivo());
        response.setClienteId(cartao.getCliente().getId());
        response.setNumero(cartao.getNumeroCartao());

        return response;
    }

    public ChangeStatusCartaoResponse ativarCartao(Cartao cartao) {
        ChangeStatusCartaoResponse response = new ChangeStatusCartaoResponse();
        response.setId(cartao.getIdCartao());
        response.setAtivo(cartao.isAtivo());
        response.setClienteId(cartao.getCliente().getId());
        response.setNumero(cartao.getNumeroCartao());

        return response;
    }

    public CartaoDetailsResponse toCartaoDetailsResponse(Cartao cartao) {
        CartaoDetailsResponse cartaoDetailsResponse = new CartaoDetailsResponse();

        cartaoDetailsResponse.setId(cartao.getIdCartao());
        cartaoDetailsResponse.setNumero(cartao.getNumeroCartao());
        cartaoDetailsResponse.setClienteId(cartao.getCliente().getId());
        cartaoDetailsResponse.setAtivo(cartao.isAtivo());

        return cartaoDetailsResponse;
    }


}
