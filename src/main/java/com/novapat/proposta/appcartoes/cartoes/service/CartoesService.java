package com.novapat.proposta.appcartoes.cartoes.service;

import com.novapat.proposta.appcartoes.cartoes.model.Cartao;

public interface CartoesService {

    Cartao save(Cartao cartao) throws Exception;

    Cartao ativar(Cartao cartao);

    Cartao findByNumeroCartao(String numeroCartao);
}
