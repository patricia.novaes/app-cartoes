package com.novapat.proposta.appcartoes.cartoes.model.dto.request;

public class CreateCartaoRequest {

    private String numero;
    private long clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }
}
