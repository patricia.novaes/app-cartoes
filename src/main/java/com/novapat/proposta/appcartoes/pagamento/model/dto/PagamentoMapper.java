package com.novapat.proposta.appcartoes.pagamento.model.dto;

import com.novapat.proposta.appcartoes.pagamento.model.Pagamento;
import com.novapat.proposta.appcartoes.pagamento.model.dto.request.CreatePagamentoRequest;
import com.novapat.proposta.appcartoes.pagamento.model.dto.response.CreatePagamentoResponse;
import com.novapat.proposta.appcartoes.pagamento.model.dto.response.DetailPagamentoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    public Pagamento pagar(CreatePagamentoRequest request) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(request.getDescricao());
        pagamento.setIdCartao(request.getCartao_id());
        pagamento.setValor(request.getValor());

        return pagamento;
    }

    public CreatePagamentoResponse criarPagamentoResponse(Pagamento pagamento) {
        CreatePagamentoResponse response = new CreatePagamentoResponse();
        response.setDescricao(pagamento.getDescricao());
        response.setId(pagamento.getIdCompra());
        response.setValor(pagamento.getValor());
        response.setCartao_id(pagamento.getIdCartao());

        return response;
    }

    public List<DetailPagamentoResponse> visualizarPagamentos(List<Pagamento> pagamentos) {
        List<DetailPagamentoResponse> response = new ArrayList<>();
        DetailPagamentoResponse detailResponse = null;

        for (Pagamento pagamento : pagamentos) {
            detailResponse = new DetailPagamentoResponse();
            detailResponse.setCartao_id(pagamento.getIdCartao());
            detailResponse.setId(pagamento.getIdCompra());
            detailResponse.setDescricao(pagamento.getDescricao());
            detailResponse.setValor(pagamento.getValor());

            response.add(detailResponse);
        }

        return response;
    }

}

