package com.novapat.proposta.appcartoes.pagamento.repository;

import com.novapat.proposta.appcartoes.pagamento.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

    List<Pagamento> findByIdCartao(Long idCartao);

}
