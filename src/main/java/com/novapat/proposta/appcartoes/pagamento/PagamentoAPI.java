package com.novapat.proposta.appcartoes.pagamento;

import com.novapat.proposta.appcartoes.pagamento.model.Pagamento;
import com.novapat.proposta.appcartoes.pagamento.model.dto.PagamentoMapper;
import com.novapat.proposta.appcartoes.pagamento.model.dto.request.CreatePagamentoRequest;
import com.novapat.proposta.appcartoes.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoAPI {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping(value = "/pagamento", consumes = "application/json")
    public ResponseEntity<?> realizarPagamento(@RequestBody CreatePagamentoRequest request) {
        try {

            Pagamento pagamento = pagamentoMapper.pagar(request);
            pagamento = pagamentoService.save(pagamento);
            return new ResponseEntity<>(pagamentoMapper.criarPagamentoResponse(pagamento), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== erro ao realizar o pagamento";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/pagamentos/{idcartao}")
    public ResponseEntity<?> visualizarExtrato(@PathVariable(name = "idcartao") long idcartao) {
        try {

            List<Pagamento> payments = pagamentoService.extrato(idcartao);
            return new ResponseEntity<>(pagamentoMapper.visualizarPagamentos(payments), HttpStatus.OK);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== visualizar extrato do pagamento";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

}
