package com.novapat.proposta.appcartoes.pagamento.service;

import com.novapat.proposta.appcartoes.pagamento.model.Pagamento;

import java.util.List;

public interface PagamentoService {

    Pagamento save(Pagamento pagamento) throws Exception;

    List<Pagamento> extrato(long idcartao);
}
